/*jshint node: true, camelcase:true*/
module.exports = function (grunt) {
    'use strict';

    var BUILD_FOLDER = 'build/';        // Temporary build folder
    var DIST_FOLDER = 'dist/';          // Final distribution folder

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: [BUILD_FOLDER, DIST_FOLDER, '.tmp'],

        copy: {
            main: {
                expand: true,
                cwd: 'app/',
                src: ['**', '!js/**', '!lib/**', '!**/*.css'],
                dest: BUILD_FOLDER
            },
            shims: {
                expand: true,
                cwd: 'app/lib/webshim/shims',
                src: ['**'],
                dest: 'build/js/shims'
            }
        },

        rev: {
            files: {
                src: [BUILD_FOLDER + '**/*.{js,css}', '!build/js/shims/**']
            }
        },

        useminPrepare: {
            html: 'index.html'
        },

        usemin: {
            html: [BUILD_FOLDER + 'index.html']
        },

        uglify: {
            options: {
                report: 'min',
                mangle: false
            }
        },

        compress: {
            main: {
                options: {
                    mode: 'gzip'
                },
                expand: true,
                cwd: 'dist/',
                src: ['**/*'],
                dest: 'dist/'
            }
        },

        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'css/styles.min.css': ['css/styles.css']
                }
            }
        }

    }),

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-rev');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-contrib-compress');

    // Tell Grunt what to do when we type "grunt" into the terminal
    grunt.registerTask('default', [
        'copy', 'useminPrepare', 'concat', 'uglify', 'rev', 'usemin', 'compress', 'cssmin'
    ]);
};