// Define global variables for JSHint

angular.module('ryanAirTestApp.services', []);

(function () {
    'use strict';

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // *** LocalizeSvc ***
    // Localization
    ///////////////////////////////////////////////////////////////////////////////////////////////
    angular.module('ryanAirTestApp.services').factory('LocalizeSvc', [
        '$rootScope',
        '$http',
        '$window',
        '$log',
        function ($rootScope, $http, $window, $log) {
            $log.log('New AngularJS Service: LocalizeSvc');
            ///////////////////////////////////////////////////////////////////////////////////////
            // Internal Variables
            ///////////////////////////////////////////////////////////////////////////////////////
            var URL_PREFIX = 'resources/i18n/resources-locale_';

            var _language = 'default';
            var _default = {};

            ///////////////////////////////////////////////////////////////////////////////////////
            // Internal Functions
            ///////////////////////////////////////////////////////////////////////////////////////
            function successCallback(data) {
                // store the returned array in the rootScope
                $rootScope.i18n = angular.copy(_default);
                angular.extend($rootScope.i18n, data);
            }

            function initLocalizedResources() {
                if (_language === 'default') {
                    $rootScope.i18n = _default;
                    return;
                }

                // build the url to retrieve the localized resource file
                var url = URL_PREFIX + _language + '.json';
                // request the resource file
                $http({ method: 'GET', url: url, cache: false }).success(successCallback).error(function () {
                    // The request failed. Use the default resources.
                    $rootScope.i18n = _default;
                });
            }

            ///////////////////////////////////////////////////////////////////////////////////////
            // Initialize default resources
            ///////////////////////////////////////////////////////////////////////////////////////
            $rootScope.i18n = {};
            $http({ method: 'GET', url: URL_PREFIX + 'default.json', cache: false }).success(function (data) {
                $rootScope.i18n = _default = data;
            });

            ///////////////////////////////////////////////////////////////////////////////////////
            // Public Interface
            ///////////////////////////////////////////////////////////////////////////////////////
            return {
                getLanguage: function () {
                    return _language;
                },
                setLanguage: function (value) {
                    value = value || $window.navigator.userLanguage || $window.navigator.language || 'default';
                    if (_language === value) {
                        return;
                    }
                    _language = value;
                    initLocalizedResources();
                }
            };
        }
    ]);
}());