// Define global variables for JSHint
/*global Utils*/
(function () {
    'use strict';

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // *** DataSvc ***
    // Fetch data from the API routes
    ///////////////////////////////////////////////////////////////////////////////////////////////
    angular.module('ryanAirTestApp.services').factory('DataSvc', [
        '$http',
        '$log',
        '$q',
        'PubSubSvc',
        function ($http, $log, $q, PubSubSvc) {
            $log.log('New AngularJS Service: DataSvc');
            ///////////////////////////////////////////////////////////////////////////////////////
            // Internal Variables
            ///////////////////////////////////////////////////////////////////////////////////////
            //var API_URL = 'http://www.ryanair.com/en/api/2/forms/flight-booking-selector/';
            var API_URL = '/airports';
            var API_CHEAP_FLIGHT = '/cheapFlight';
            var _airportCodes = {};
            var _latestResults = [];

            ///////////////////////////////////////////////////////////////////////////////////////
            // Internal Functions
            ///////////////////////////////////////////////////////////////////////////////////////

            ///////////////////////////////////////////////////////////////////////////////////////
            // Initialize default resources
            ///////////////////////////////////////////////////////////////////////////////////////
            $http({ method: 'GET', url: API_URL, cache: false, headers: {'Content-Type': 'application/json'} }).success(function (data) {
                _airportCodes = data;
                PubSubSvc.publish('getAllAirports', [_airportCodes.airports]);
            });

            ///////////////////////////////////////////////////////////////////////////////////////
            // Public Interface
            ///////////////////////////////////////////////////////////////////////////////////////
            return {
                getAirports: function () {
                    return _airportCodes.airports;
                },

                getLatestResults: function () {
                    return _latestResults;
                },


                findFlight: function (origin, destination, from, to) {
                    var data = {
                        origin: origin,
                        destination: destination,
                        from: Utils.dateFormater(from),
                        to: Utils.dateFormater(to),
                    };

                    var dataRet = {
                        origin: destination,
                        destination: origin,
                        from: Utils.dateFormater(from),
                        to: Utils.dateFormater(to),
                    };

                    var defer = $q.defer();
                    _latestResults = [];

                    $http({ method: 'POST', url: API_CHEAP_FLIGHT, cache: false, headers: {'Content-Type': 'application/json'}, data: data}).then(function (result) {
                        if (result.data.flights && result.data.flights.length > 0) {
                            _latestResults.push(result.data.flights[0]);
                        }
                        data.from = Utils.dateFormater(from, 1);
                        data.to = Utils.dateFormater(to, 1);
                        return $http({ method: 'POST', url: API_CHEAP_FLIGHT, cache: false, headers: {'Content-Type': 'application/json'}, data: data});
                    }).then(function (result) {
                        if (result.data.flights && result.data.flights.length > 0) {
                            _latestResults.push(result.data.flights[0]);
                        }
                        data.from = Utils.dateFormater(from, 2);
                        data.to = Utils.dateFormater(to, 2);
                        return $http({ method: 'POST', url: API_CHEAP_FLIGHT, cache: false, headers: {'Content-Type': 'application/json'}, data: data});
                    }).then(function (result) {
                        if (result.data.flights && result.data.flights.length > 0) {
                            _latestResults.push(result.data.flights[0]);
                        }
                        return $http({ method: 'POST', url: API_CHEAP_FLIGHT, cache: false, headers: {'Content-Type': 'application/json'}, data: dataRet});
                    }).then(function (result) {
                        if (result.data.flights && result.data.flights.length > 0) {
                            _latestResults.push(result.data.flights[0]);
                        }
                        dataRet.from = Utils.dateFormater(from, 1);
                        dataRet.to = Utils.dateFormater(to, 1);
                        return $http({ method: 'POST', url: API_CHEAP_FLIGHT, cache: false, headers: {'Content-Type': 'application/json'}, data: dataRet});
                    }).then(function (result) {
                        if (result.data.flights && result.data.flights.length > 0) {
                            _latestResults.push(result.data.flights[0]);
                        }
                        dataRet.from = Utils.dateFormater(from, 2);
                        dataRet.to = Utils.dateFormater(to, 2);
                        return $http({ method: 'POST', url: API_CHEAP_FLIGHT, cache: false, headers: {'Content-Type': 'application/json'}, data: dataRet});
                    }).then(function (result) {
                        if (result.data.flights && result.data.flights.length > 0) {
                            _latestResults.push(result.data.flights[0]);
                        }
                        defer.resolve();
                    });
                    return defer.promise;
                }
            };
        }
    ]);
}());