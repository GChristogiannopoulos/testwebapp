// Define global variables for JSHint

(function () {
    'use strict';

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // *** PubSubSvc ***
    // Publish-Subscribe service used by Angular controllers and services
    ///////////////////////////////////////////////////////////////////////////////////////////////
    angular.module('ryanAirTestApp.services').factory('PubSubSvc', [
        '$log',
        function ($log) {
            $log.log('New AngularJS Service: PubSubSvc');

            ///////////////////////////////////////////////////////////////////////////////////////
            // Internal Variables
            ///////////////////////////////////////////////////////////////////////////////////////
            var _cache = {};

            ///////////////////////////////////////////////////////////////////////////////////////
            // Public Interface
            ///////////////////////////////////////////////////////////////////////////////////////
            return {
                publish: function (topic, args) {
                    _cache[topic] && _cache[topic].forEach(function (item) {
                        try {
                            args = (args instanceof Array) ? args : [args];
                            item.apply(null, args || []);
                        } catch (e) {
                            $log.error(e);
                        }
                    });
                },
                subscribe: function (topic, callback) {
                    if (!_cache[topic]) {
                        _cache[topic] = [];
                    }
                    _cache[topic].push(callback);
                },
                unsubscribe: function (topic, callback) {
                    if (_cache[topic]) {
                        var idx = _cache[topic].indexOf(callback);
                        if (idx >= 0) {
                            _cache[topic].splice(idx, 1);
                        }
                    }
                }
            };
        }
    ]);
}());