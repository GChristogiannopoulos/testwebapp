// Define global variables for JSHint

angular.module('ryanAirTestApp.controllers', []);

(function () {
    'use strict';

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // *** AppCtrl ***
    //
    // Manages page navigation
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    angular.module('ryanAirTestApp.controllers').controller('AppCtrl', [
        '$scope',
        '$log',
        'LocalizeSvc',
        '$location',
        '$route',
        function ($scope, $log, LocalizeSvc, $location, $route) {
            $log.log('New AngularJS Controller: AppCtrl');

            ///////////////////////////////////////////////////////////////////////////////////////////////
            // scope functions
            ///////////////////////////////////////////////////////////////////////////////////////////////
            $scope.go = function (path, urlParams) {
                $log.log('[AppCtrl]: Route request to ' + path);

                // Remove search parameters such as ?item
                $location.url($location.path());

                path = path.replace('#', '');
                if ($location.path() === path) {
                    $route.reload();
                } else {
                    $location.path(path);
                }

                // Add custom search parameters to url
                if (urlParams) {
                    var keys = Object.keys(urlParams);
                    keys.forEach(function (key) {
                        $location.search(key, urlParams[key]);
                    });
                }
            };

            $scope.open = function ($event, opened) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope[opened] = true;
            };
        }
    ]);
})();