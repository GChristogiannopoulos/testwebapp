// Define global variables for JSHint
(function () {
    'use strict';

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // *** FindViewCtrl ***
    //
    // Manages the medicines searchable list
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    angular.module('ryanAirTestApp.controllers').controller('FindViewCtrl', [
        '$scope',
        '$log',
        'DataSvc',
        'PubSubSvc',
        function ($scope, $log, DataSvc, PubSubSvc) {
            $log.log('New AngularJS Controller: FindViewCtrl');
            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Internal variables and functions
            ///////////////////////////////////////////////////////////////////////////////////////////////
            var tomorrow = new Date();
            var afterTomorrow = new Date();

            ///////////////////////////////////////////////////////////////////////////////////////////////
            // PubSubSvc listeners
            // (You MUST unsubscribe to these events on the $destroy event handler below)
            ///////////////////////////////////////////////////////////////////////////////////////////////
            function getAllAirports(airports) {
                $scope.airports = airports;
            }

            PubSubSvc.subscribe('getAllAirports', getAllAirports);

            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Event listeners
            ///////////////////////////////////////////////////////////////////////////////////////////////
            $scope.$on('$destroy', function () {
                PubSubSvc.unsubscribe('getAllAirports', getAllAirports);
            });

            ///////////////////////////////////////////////////////////////////////////////////////////////
            // scope functions
            ///////////////////////////////////////////////////////////////////////////////////////////////

            $scope.submitSearch = function () {
                if (!$scope.originAirport || !$scope.destinationAirport) {
                    $scope.formSubmitted = true;
                    return;
                }
                var origin = $scope.originAirport.originalObject.iataCode;
                var destination = $scope.destinationAirport.originalObject.iataCode;
                $scope.inProgress = true;
                DataSvc.findFlight(origin, destination, $scope.departingOn, $scope.returningOn).then(function () {
                    $scope.go('/result/');
                });
            };

            $scope.today = function () {
                $scope.departingOn = new Date();
                $scope.returningOn = tomorrow;
            };

            $scope.clear = function () {
                $scope.departingOn = null;
                $scope.arrivingOn = null;
            };

            // Disable weekend selection
            $scope.disabled = function (date, mode) {
                return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
            };

            $scope.toggleMin = function () {
                $scope.minDate = $scope.minDate ? null : new Date();
            };

            $scope.getDayClass = function (date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }
                return '';
            };

            ///////////////////////////////////////////////////////////////////////////////////////////////
            // Initializations
            // Keep this at the end of the controller.
            ///////////////////////////////////////////////////////////////////////////////////////////////

            $scope.today();
            $scope.toggleMin();
            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };
            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            tomorrow.setDate(tomorrow.getDate() + 1);
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            $scope.events =
            [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            $scope.airports = DataSvc.getAirports();

            $scope.inProgress = false;
        }
    ]);
})();