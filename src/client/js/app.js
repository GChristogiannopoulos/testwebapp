// Define global variables for JSHint

(function () {
    'use strict';
    angular.module('ryanAirTestApp', ['ryanAirTestApp.services', 'ryanAirTestApp.directives', 'ryanAirTestApp.controllers', 'ui.bootstrap', 'ngRoute'])
    .config(['$routeProvider', '$httpProvider', '$sceProvider', function ($routeProvider, $httpProvider, $sceProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/findView.html',
                controller: 'FindViewCtrl'
            })
            .when('/result', {
                templateUrl: 'views/resultsView.html',
                controller: 'ResultsViewCtrl'
            })
            .otherwise({
                redirectTo: '/'
            }
        );
        $sceProvider.enabled(false);
    }])
    .run(['$rootScope', '$log', function ($rootScope, $log) {
        $log.log('Cheap flight finder application');
    }]);
}());