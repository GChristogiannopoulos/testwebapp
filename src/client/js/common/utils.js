/*exported Utils*/
var Utils = {};

Utils.dateFormater = function (date, incrementDate) {
    'use strict';
    incrementDate = incrementDate ? incrementDate : 0;
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + (date.getDate() + incrementDate);
};