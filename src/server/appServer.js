/*global require, console*/
var HTTP_PORT = 9000;

(function () {
    'use strict';

    // Load Third Party Modules
    var express = require('express');
    var http = require('http');
    var app = express();

    var compression = require('compression');

    app.use(compression());
    console.log('\n\nStarting HTTP Server...\n');

    var htmlConfig = require('./js/htmlConfig');
    var httpServer = http.createServer(app);
    httpServer.listen(HTTP_PORT);

    var clientPathToServe = '/../../client/';
    // Configure Express and setup routes for app
    htmlConfig(app, express, clientPathToServe);

    console.log('\n\nHTTP Server is listening in port ' + HTTP_PORT + '\n');
})();
