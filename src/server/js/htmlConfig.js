/*global require, module, __dirname, console*/
(function () {
    'use strict';

    module.exports = function (app, express, path) {

        //configure for cross-domain
        app.use(function (req, res, next) {
            res.header('Access-Control-Allow-Credentials', true);
            res.header('Access-Control-Allow-Origin', req.headers.origin);
            res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
            res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
            res.header('Accept-Encoding', 'gzip');
            next();
        });

        // config middleware
        var errorHandler = require('errorhandler');
        var request = require('request');
        var bodyParser = require('body-parser');

        app.use(errorHandler({dumpExceptions: true, showStack: true}));
        app.use(express['static'](__dirname + path + '/'));
        app.use(bodyParser());

        app.get('/', function (req, res) {
            console.log('Received GET request to ' + req.url);
            res.render('index.html');
        });

        app.get('/airports', function (req, res) {
            console.log('Received GET request to ' + req.url);
            console.log('get airports');
            request('http://www.ryanair.com/en/api/2/forms/flight-booking-selector', function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(body);
                    res.end();
                }
            });
        });

        app.post('/cheapFlight', function (req, res) {
            console.log('Received POST request to ' + req.url);
            var origin = req.body.origin;
            var destination = req.body.destination;
            var from = req.body.from;
            var to = req.body.to;

            var url = 'http://www.ryanair.com/en/api/2/flights/from/' + origin + '/to/' + destination + '/' + from + '/' + to + '/250/unique/?limit=15&offset-0';
            console.log(url);
            request(url, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(body);
                    res.end();
                }
            });
        });
    };
})();